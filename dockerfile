FROM node:16.13.1 as build
WORKDIR /usr/src/app
COPY . .
RUN npm install -g @angular/cli
RUN npm install

ENV SRV_URL: "https://deploybackk.herokuapp.com/api/tutorials"
RUN ng build

FROM nginx:latest
COPY --from=build /usr/src/app/dist/mds-dds-exam-angular /usr/share/nginx/html
ENTRYPOINT nginx -g 'daemon off;'
